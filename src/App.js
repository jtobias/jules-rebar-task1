import { BrowserRouter as Router } from 'react-router-dom';
import { Container } from 'react-bootstrap'
import { Route, Routes } from 'react-router-dom';


import AppNavbar from './components/AppNavbar';
import Banner from './components/Banner';
import Services from './components/Services';
import About from './components/About';

import './App.css';

function App() {


  return (
    
      
    <Router >
      <AppNavbar />
      <Banner />
      <Services />
      <About />
    </Router>
    


  );
}

export default App;
