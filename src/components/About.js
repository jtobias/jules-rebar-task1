import { Image, Container, Row, Col, Button } from "react-bootstrap";


export default function About() {


	return (

    
        <Container fluid>

           
            <Row className="d-flex align-items-center justify-content-center p-5 about">
                
                <Col className="p-5 mt-5" md={12} lg={6}>
                    <p>Nulla facilisi cras fermentum odio eu feugiat.</p>
                    <h4 className="services-header mt-5 text-uppercase fw-bolder fs-1">Lorem Impsum</h4>
                    <p className="services-text mt-5">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nulla facilisi cras fermentum odio eu feugiat. Suspendisse ultrices gravida dictum fusce ut placerat. Viverra orci sagittis eu volutpat. Cursus mattis molestie a iaculis. Tellus in metus vulputate eu scelerisque felis imperdiet. Morbi leo urna molestie at elementum eu facilisis.</p>
                    <Button variant="light" className="mt-3 px-5 text-uppercase" type="submit">View More</Button>
                </Col>

                <Col className="text-center mt-5" md={12} lg={6}>
                    <span><Image src="https://www.rollingstone.com/wp-content/uploads/2020/10/Microsoft-Surface-Laptop-3.jpg" fluid/></span>
                </Col>

            </Row>
        
        </Container> 

    

			



	)
}