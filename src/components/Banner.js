import { Image } from "react-bootstrap"


export default function Banner() {


	return (


        
        <div className="d-flex align-items-center justify-content-center banner">
			<div className="p-5 text-center mt-5 col-md-6">
				<h1 className="banner-header text-light">Creative Solutions</h1>
				<p className="banner-text text-light mt-5">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Orci sagittis eu volutpat odio facilisis. Gravida arcu ac tortor dignissim convallis aenean et.</p>
                <p className="banner-text text-light mt-5">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Orci sagittis eu volutpat odio facilisis. Gravida arcu ac tortor dignissim convallis aenean et.</p>
                <p className="banner-text text-light mt-5">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Orci sagittis eu volutpat odio facilisis. Gravida arcu ac tortor dignissim convallis aenean et.</p>
			</div>
		</div>

	)
}