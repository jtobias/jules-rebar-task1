import { Image, Container, Row, Col } from "react-bootstrap";


export default function Services() {


	return (

        
        <Container fluid>  
            <Row className="d-flex align-items-center justify-content-center px-5 service bg-light">
                
                <Col className="p-3 text-center mt-5" sm={12} md={6} lg={3}>
                    <span className="logo"><Image src="https://as2.ftcdn.net/v2/jpg/01/16/73/87/1000_F_116738779_gCKcE4G54yTZlR32HXWRPaEoS88JOltu.jpg" roundedCircle width={150} height={150}/></span>
                    <h4 className="services-header text-primary mt-5">Consulting</h4>
                    <p className="services-text mt-5">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nulla facilisi cras fermentum odio eu feugiat. Suspendisse ultrices gravida dictum fusce ut placerat. Viverra orci sagittis eu volutpat. Cursus mattis molestie a iaculis. Tellus in metus vulputate eu scelerisque felis imperdiet. Morbi leo urna molestie at elementum eu facilisis.</p>
                </Col>

                <Col className="p-3 text-center mt-5" sm={12} md={6} lg={3}>
                    <span className="logo"><Image src="https://as1.ftcdn.net/v2/jpg/00/69/14/06/1000_F_69140602_bH9LubfGrz5jriBcDk8k13hC7nYOAmZb.jpg" roundedCircle width={150} height={150}/></span>
                    <h4 className="services-header text-primary mt-5">Developing</h4>
                    <p className="services-text mt-5">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nulla facilisi cras fermentum odio eu feugiat. Suspendisse ultrices gravida dictum fusce ut placerat. Viverra orci sagittis eu volutpat. Cursus mattis molestie a iaculis. Tellus in metus vulputate eu scelerisque felis imperdiet. Morbi leo urna molestie at elementum eu facilisis.</p>
                </Col>

                <Col className="p-3 text-center mt-5" sm={12} md={6} lg={3}>
                    <span className="logo"><Image src="https://as2.ftcdn.net/v2/jpg/02/03/47/23/1000_F_203472348_6tSV5QW4EuYETdILZDiiOWWbYJ56YUzU.jpg" roundedCircle width={150} height={150}/></span>
                    <h4 className="services-header text-primary mt-5">Designing</h4>
                    <p className="services-text mt-5">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nulla facilisi cras fermentum odio eu feugiat. Suspendisse ultrices gravida dictum fusce ut placerat. Viverra orci sagittis eu volutpat. Cursus mattis molestie a iaculis. Tellus in metus vulputate eu scelerisque felis imperdiet. Morbi leo urna molestie at elementum eu facilisis.</p>
                </Col>

                <Col className="p-3 text-center mt-5" sm={12} md={6} lg={3}>
                    <span className="logo"><Image src="https://thumbs.dreamstime.com/z/white-line-magnifying-glass-data-analysis-icon-isolated-blue-background-search-sign-long-shadow-vector-274572135.jpg" roundedCircle width={150} height={150}/></span>
                    <h4 className="services-header text-primary mt-5">Analysis</h4>
                    <p className="services-text mt-5">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nulla facilisi cras fermentum odio eu feugiat. Suspendisse ultrices gravida dictum fusce ut placerat. Viverra orci sagittis eu volutpat. Cursus mattis molestie a iaculis. Tellus in metus vulputate eu scelerisque felis imperdiet. Morbi leo urna molestie at elementum eu facilisis.</p>
                </Col>

            </Row>
        </Container>  

			



	)
}