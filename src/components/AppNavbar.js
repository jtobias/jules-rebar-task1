import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Link, NavLink } from 'react-router-dom';


export default function AppNavbar() {


	return (

		<Navbar bg="light" expand="lg" data-bs-theme="light">
			<Container fluid>
				<Navbar.Brand as={ NavLink } to="/" className="text-dark ms-5 text-uppercase fw-bold">Softgray</Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav" />
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="ms-auto me-5">
						<Nav.Link className="navlinks text-dark me-3 text-uppercase" as={ NavLink } to="/">Products</Nav.Link>
						<Nav.Link className="navlinks text-dark me-3 text-uppercase" as={ NavLink } to="/">Services</Nav.Link>
						<Nav.Link className="navlinks text-dark me-3 text-uppercase" as={ NavLink } to="/">About</Nav.Link>
						<Nav.Link className="navlinks text-dark me-3 text-uppercase" as={ NavLink } to="/">Jobs</Nav.Link>
						<Nav.Link className="navlinks text-dark me-3 text-uppercase" as={ NavLink } to="/">Contact</Nav.Link>
						<Nav.Link className="text-dark me-3 fw-bold" as={ NavLink } to="/">+81-00-0000-0000</Nav.Link>
					</Nav>
				</Navbar.Collapse>
			</Container>
		</Navbar>

	)
}